'use strict';

var gulp = require('gulp');

var browserSync = require('browser-sync').create();


// var imagemin = require('gulp-imagemin');
// var sourcemaps = require('gulp-sourcemaps');
// var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');


var path = require('path');


var processors = [
    autoprefixer({
        browsers: [
            'last 3 versions',
            '> 1%',
            'opera 12.1',
            'bb 10',
            'android 4'
        ]
    })
];

gulp.task('html', function () {
    gulp.src('src/*.html')
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());

});

gulp.task('all', function () {
    gulp.src('src/*/**')
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());

});


gulp.task('serve', [
    'html', 'all'

], function () {
    browserSync.init({
        startPath: 'index.html',
        server: {
            baseDir: './dist',
            routes: {
                "/node_modules": "node_modules",
		        "/bower_components" : "bower_components"
            }
        }
    });

});


gulp.task('default', function () {
    gulp.start('serve')
});
