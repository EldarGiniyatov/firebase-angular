# Front-end part

### Описание основных моментов для работы:

 1. Установка пакетов для полноценной работы
 1. Как ипользовать установленные JS библиотеки и подключать свои
 1. Редактирование CSS стилей
 1. Cackle комментарии.

## 1. Установка пакетов для полноценной работы

Установка NVM

> `NVM` (Node.js version manager) - менеджер версий Node.js. NVM позволяет установить несколько версий Node.js, что дает возможность использовать новую версию Node.js, но при
> необходимости работать и с более старой версией Node.js, если приложение не работает с новой версией, либо по каким-то другим причинам. 
  
> Документация для установки - [NVM install][nvm_install]
  
Так же вам необходимо установить `node.js` & `npm.js` `bower.js` для установки js библиотек, версия этих пакетов должна быть не ниже:

```json
 "node": ">=10.0.0",
 "npm": "6.4.x",
 "bower" "1.8.2"
```

Установите через командую строку `npm` & `bower` глобально используя префикс `-g`: 

```
 $ npm install npm@latest -g
 $ npm install bower@latest -g
```

* Теперь нам доступен `bower`, запускаем команду `$ bower install` - тем самым установим все JS библиотеки (зависимости) из файла [bower.json](../../bower.json), которые используются на проекте.

> Для того, чтоб добавить свою библиотеку в файл [bower.json](../../bower.json) выполните команду: `bower install имя_библиотеки`.


# 2) Как ипользовать установленные JS библиотеки и подключать свои:

Все зависимости установятся в папку `public/components`, путь можно изменить в конфигурационном файле bower.json тут: [.bowerrc](../../.bowerrc).

> Для подключение любого JS файла или библиотеки на проекте мы используем библиотеку `RequireJS`.
  `RequireJS` - js библиотека позволяющая разбивать код на chunks(кусочки) и подключать только использующий код на странице. 
   RequireJS использует асинхронную модель подгрузки зависимостей – AMD. 
   
Документация: [requireJS][requireJS].


Внимание! На проекте используются 2 файла конфигурации библиотеки `RequireJS`:

*  Для фронтенда [public/assets/frontend/js/config.js](public/assets/frontend/js/config.js)
*  Для админки [public/assets/admin-new/js/config.js](../../public/assets/admin/js/config.js)
  
Файл `config.js` содержит описаение JS объекта `require`.

Структура объекта `require`, который содержит следующие св-ва: 

* в свойстве `paths` - объект, в котором описываются алиасы путей к включаемым JS файлам.

* в свойстве `deps` - подключаются зависимости необходомые для всех страниц.

* в свойстве `shims` - позволяет добавить сторонние модули(которые определены не в AMD стиле, или проще говоря: без метода define) к общей схеме.

----

Св-ва объекта `paths` можно условно разделить на группы. Обратите внимания на `naming`, мы используем `_` для разделителя. 

* `Core` - нету префикса
* `Pages` - префикс `page_`
* `Components` - префикс `component_`
* `Controllers` - префикс `controller_`
* `Libs` - префикс `lib_`

```js
// Пример:
var require = {
    paths: {
        // App - содержит JS который используется на всех страницах
        core: '/assets/frontend/js/core',

        // Components - содержит js компонеты например button, switcher-tabs
        component_switcher_tabs: '/assets/frontend/js/components/component_switcher_tabs',
        component_button: '/assets/frontend/js/components/component_button',

        // Controllers - содержит контроллеры занимающиеся логикой приложения (обработкой событий и тд)
        controller_order: '/assets/frontend/js/controllers/controller_order',
        controller_feedback: '/assets/frontend/js/controllers/controller_feedback',
        
        // Pages - могут содержать в себе как Components так и Controllers, который мы можем подключить для конкретных страниц(шаблонов)
        page_index: '/assets/frontend/js/pages/page_index',
        page_articles: '/assets/frontend/js/pages/page_articles',
        page_pages: '/assets/frontend/js/pages/page_pages',

        // Libs - стороние библиотеке которые мы установили через bower
        lib_jquery: '/components/jquery/dist/jquery.min',
        lib_lodash: '/components/lodash/dist/lodash.min',
    },
    // Тут подключаются модули(которые определены не в AMD стиле, или проще говоря: без метода define)
     shim: {
        JSONEditor: {
            deps: ['css!/components/jsoneditor/dist/jsoneditor.min.css']
        },
     },
     // В deps мы можем указать JS который мы хотим использовать на всех страницах
     deps: ['core', 'lib_jquery']
};
```

В файл [resources/views/layouts/partials/scripts.blade.php](../../resources/views/layouts/partials/scripts.blade.php) подключаем RequireJS : 

```js
<script type="text/javascript" src="{{ asset('/assets/frontend/js/config.js') }}"></script>
<script type="text/javascript" src="{{ asset('/components/requirejs/require.js') }}"
        data-main="{{ $requirejsPage or 'app' }}"></script>
```

В атрибут `data-main` передаем алиас, который указывает какой файл мы подгружаем из `config.js`. 

В примере ниже рассматриваем всё тот же `config.js` файл, в котором `page_index` будет точкой входа. Eё мы и вставим в `data-main` атрибут.

Пример использования модульного подхода на ReruireJS:
```js
// Файл `config.js`
// Создаем алиас `page_index`, который будет содержать наши модули
var require = {
  paths: {
      // Pages
      page_index: '/assets/frontend/js/pages/page_index',
      
      // Components
      component_sum1: '/assets/frontend/js/components/component_sum1',
      component_mul2: '/assets/frontend/js/components/component_mul2',
  }
}
```

```js
// Файл component_sum1.js - это модуль, в котором определена функция, которую мы возвращаем 
define(function(){
    return function(a, b) {
        console.log(a + b);
    }
})

// Файл component_mul2.js - еще один модуль.
define(function(){
    return function(a, b) {
        console.log(a * b);
    }
})

// Для примера подключим наши модули в файл `page_index.js`:
define(['component_sum1', 'component_mul2'], function(component_sum1, component_mul2){
    // Тут мы можем использовать функционал всех подключенных модулей
    component_sum1(10, 4) // 14
    component_mul2(2, 2) // 4
});
    
``` 

Осталось только подключить наш `page_index` в blade-template страницы:
```php
@extends('layouts.default', ['requirejsPage' => 'page_index'])
```


# 3) Редактирование CSS стилей:

> На проекте для стилей используется препроцессор `SASS(.scss)`, 
с помощью webpack мы компилируем все `.scss` файлы в один bundle: [public/assets/frontend/css/app.min.css](../../public/assets/frontend/css/app.min.css).

Для установки webpack и loaders (необходмых для компиляции `.js` & `.scss` файлов) выполним команду:

* `$ npm install`  - устанавливаем зависимости из файла [package.json](../../package.json)

После этого мы можем запустить сборку:
* `$ npm run build`  - сборка проекта по файлу конфигурации [webpack.config.js](../../webpack.config.js)
* `$ npm run watch`  - запускаем `watcher`, который следит за измененим файлов в нашем проекте и пересобирает его,
 так же одновременно с ним запускается `livereload` режим позволяющий не обновлять браузер после того, как мы обновили стили.
* `$ npm run gulp`  -  запускаем `watcher`, который следит за измененим `js` файлов. Настройки можно посмотреть в `gulpfile.js` файле.

`livereload` - это специальный плагин для webpack `LiveReloadPlugin`, как подключается можно посмотреть в [webpack.config.js](../../webpack.config.js).

Осталось только подключить `livereload` в наш темплейт в файл [resources/views/layouts/partials/scripts.blade.php](../../resources/views/layouts/partials/scripts.blade.php)

```php
@if(isDevelopment())
    <script src="http://localhost:35729/livereload.js"></script>
@endif
```

Документация: [webpack][webpack].

> Используем условие `isDevelopment`, т.к. этот js нужен только на dev.


> Стили добавляем и редактируем тут: [resources/assets/scss/](resources/assets/scss/).

Рассмотрим подбробнее структуру папок для `scss` файлов:

```js
├── scss
│   ├── base
│   ├── components
│   ├── pages
│   ├── partials
│   ├── utils
│   └── vendor
|   └── app.scss

```

Название папки  | Содержание файла
----------------|----------------------
base            | Основые стили для всего сайта, шрифты, хелперы, стили для печати
components      | Компонеты кнопки, карусели, формы и т.д.
pages           | Стили для конкретных страниц и общие которые используются везде
partials        | Основые секции такие как header, footer, nav, sidebar
utils           | Стили для mixins, variables & normalize
vendor          | Стили для стороних библиотек 
app.scss        | Файл в который импортируем все наши `.scss` файлы стилей (только те файлы которые подлючены в этом файле попадут в конечную сборку)


Все `.scss` файлы собираются в один файл [resources/assets/scss/app.scss](../../resources/assets/scss/app.scss)
Благодаря этому мы можем подключать любые стили для стороних библиотек установленных c помощью `bower` или `npm`.

Как пример покажу на библиотеке `bootstrap` установленной через `npm`. Чтобы не тянуть весь `bootstrap`, 
я подключил в мой файл `app.scss` только `bootstrap-grid`. Собираются стили всё тем же `webpack`.

```scss 
// Bootstrap Grid
@import "../../../node_modules/bootstrap/scss/bootstrap-grid";
```

> На проект используется bootstrap-grid (v4), подробнее можно ознакомиться тут [bootstrap][bootstrap]


И на последок, в файле [package.json](../../package.json) мы можем указываеть переменную `NODE_ENV` - это позволяет
запускать скрипты как для `develepment` версии, так и для `production` версии. Это нужно для того, чтобы на `production` версии не
запускались `watch`, `liveroload`, `sourcemaps` и т.д., тем самым ускоряем сборку и уменьшаем конечный бандл.

Пример:

```js
 "scripts": {
    // Сборка проекта для dev
    "build": "webpack --progress --hide-modules",
    
    // Сборка проекта для dev + watcher + livereload //
    "watch": "NODE_ENV=development webpack --progress --hide-modules --watch",
    
    // Сборка проекта JS файлов, минификация + livereload//
    "gulp": "gulp watch",
    
    // Сборка проекта для prod
    "prod": "NODE_ENV=production webpack --progress --hide-modules --config webpack.production.config.js"
 }
```
        
* Команда `npm run prod` - собирёт файлы для production версии.

## 4. Cackle-комментарии

На данный момент комментарии реализованы с помощью сервиса [Cackle].

Для подключения раздела комментариев используется blade-шаблон `components.cackle.comments`
с обязательным параметром `channel_id` в качестве указателя канала сообщений.

Для отрисовки рейтинга используется blade-шаблон `components.cackle.comments`
с обязательным параметром `channel_id` в качестве указателя канала сообщений.

Авторизация производится при помощи helper-а `cackeSingleAuthToken` который получает пользователя из guard-а `web`.
Для отрисовки аватара, используется gravatar


[nvm_install]:https://xn--d1acnqm.xn--j1amh/%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D0%B8/%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0-nodejs-%D0%B2-ubuntu-%D0%BF%D1%80%D0%B8-%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D0%B8-nvm
[bootstrap]:http://v4-alpha.getbootstrap.com/layout/overview/
[requireJS]:http://requirejs.org/
[webpack]:https://webpack.js.org/
[Cackle]:http://cackle.me

