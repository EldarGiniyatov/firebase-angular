var myApp = angular.module('main', ['firebase']);


myApp.controller('mainCtrl', function ($scope, $firebaseAuth, $firebaseArray, $firebaseObject) {

    var currentUid = window.localStorage["currentIdentity"];
    var _ref = firebase.database().ref('users/' + currentUid);
    
    var obj = $firebaseObject(_ref);

    obj.$loaded().then(function(){
        $scope.username = obj.username;
        console.log('username', obj.username);
        $scope.avatar = obj.base64_profile_picture
    });
})