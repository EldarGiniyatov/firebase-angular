var myApp = angular.module('registration', ['firebase', 'naif.base64']);

function getFileExtension(fullFileName) {
    var regex = /(\.){1}([A-z]+)$/;
    var match = regex.exec(fullFileName);

    if (match == null) {
        throw Error("Can't find extension for file.");
    }

    return match[0];
}

function base64ToArrayBuffer(base64String) {
    var binary = window.atob(base64String);
    var bufferLength = binary.length;
    var array = new Uint8Array(bufferLength);

    for (var i = 0; i < bufferLength; i++) {
        array[i] = binary.charCodeAt(i);
    }

    return array.buffer;
}

myApp.controller('registerCtrl', function ($scope, $firebaseAuth, $location) {

    $scope.register = function () {
        var auth = $firebaseAuth();

        auth.$createUserWithEmailAndPassword($scope.email, $scope.password, $scope.username)
            .then(function (firebaseUser, $emailVerified) {

                function writeUserData(userId, name, email, base64Avatar, nameOfAvatarFile, callback) {

                    firebase.database().ref('users/' + userId).set({
                        username: name,
                        email: email
                    });

                    if (base64Avatar && base64Avatar.length > 0) {
                        //var fileExt = getFileExtension(nameOfAvatarFile);
                        var storageRef = firebase.storage().ref();
                        var avatarsRef = storageRef.child('Avatars/' +userId + '/' + nameOfAvatarFile);
                        var imageBuffer = base64ToArrayBuffer(base64Avatar);
                        
                        avatarsRef.put(imageBuffer)
                            .then(callback);
                    }
                }

                if ($emailVerified == false) {
                    console.log("need registration!");
                    $location.path('/registration');
                } else {
                    console.log("going to login page!");

                    var callback = function () {
                        $location.path('/login');
                    };

                    if ($scope.avatar) {
                        writeUserData(firebaseUser.uid, $scope.name, $scope.email, $scope.avatar.base64, $scope.avatar.filename, callback);
                    }
                    else {
                        writeUserData(firebaseUser.uid, $scope.name, $scope.email);
                    }

                    $location.path('/login');
                }

            }).catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;

                console.error(errorMessage);
                // ...
            });
    }
})