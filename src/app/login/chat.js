var myApp = angular.module('chat', ['$scope', 'firebase']);


myApp.controller('chatCtrl', function($scope, $firebase) {
        var ref = new Firebase('https://angularfire.firebaseio.com/chat');
        $scope.messages = $firebase(ref.limit(15));
        $scope.username = 'Guest' + Math.floor(Math.random()*101);
        $scope.addMessage = function() {
            $scope.messages.$add({
                from: $scope.username, content: $scope.message
            });
            $scope.message = "";
        }});

