var myApp = angular.module('login', ['firebase']);


myApp.controller('loginCtrl', function ($scope, $firebaseAuth, $location) {

    $scope.isLoginSuccess = false;

    // $scope.init = function () {
    //     console.log('Auth true loginCtrl');
    //     console.log($location);
    // }

    $scope.login = function () {

        var auth = $firebaseAuth();


        auth.$signInWithEmailAndPassword($scope.email, $scope.password).then(function (firebaseUser, $emailVerified) {

            if ($emailVerified == true) {
                console.log("auth!");
                $location.path('/main');
            } else {
                window.localStorage['currentIdentity'] = firebaseUser.uid;
                console.log("logged!");
                $location.path('/main');
            }

            console.log("Signed in as:", firebaseUser.uid);

        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;

            console.error(errorMessage);
            // ...
        });
    }

})
