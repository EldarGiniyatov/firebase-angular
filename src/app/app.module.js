
var myApp = angular.module('myApp', ['ngRoute', 'firebase', 'registration', 'login', 'main', 'flow']);


myApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.when('/registration', {
            templateUrl: './app/registration/registration.html',
            controller: 'registerCtrl'

        }).when('/login', {
            templateUrl: './app/login/login.html',
            controller: 'loginCtrl'

        }).when('/main', {
            templateUrl: './app/chat-room/chat-room.html',
            controller: 'mainCtrl'

        }).when('/chat', {
            templateUrl: './app/login/chat.html',
            controller: 'chatCtrl'

        }).when('/flow', {
            templateUrl: './app/upload-file/upload-file.html'

        }).otherwise({
            redirectTo: '/registration'
        })

    }]);

