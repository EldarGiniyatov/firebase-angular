'use strict';


angular.module('phoneList2').component('phoneList2', {
    templateUrl: 'app/phone-list/phone-list.template.html',
    controller: function PhoneListController($http) {
        var self = this;
        self.orderProp = 'age';

        $http.get('app/phone-list/phone-list.json').then(function(response) {
            self.phones = response.data;
        });
    }
});


// Register `phoneList` component, along with its associated controller and template
// angular.
// module('phoneList').
// component('phoneList', {
//     templateUrl: 'app/phone-list/phone-list.template.html',
//     controller: function PhoneListController() {
//         this.phones = [
//             {
//                 name: 'Yaroslav V.',
//                 tel: '8-901-9-25-44-24',
//                 age: 30
//             },
//             {
//                 name: 'Andrey C.',
//                 tel: '8-908-9-25-44-19',
//                 age: 24
//             }, {
//                 name: 'Eldar G.',
//                 tel: '8-928-9-25-44-66',
//                 age: 28
//             }
//         ];
//     }
// });